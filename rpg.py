import random
weapon = 1
shield = 1
hp_hero = 50
magic_level = 1
heal = 30
fireball = 50
money = 0
experience = 0
troll_hp = 10
goblin_hp = 13
ogre_hp = 25
giant_hp = 50
rage = 0    # rage points give temporary abilities. gain rage points when getting damage from enemy
confidence = 0
enemy_hp = 0

# Ctrl + shift + l ändra alla markerade variabler


class enemy:

    def __init__(self, name, hp, exp, money, damage, protection):
        self.name = name
        self.hp = hp
        self.exp = exp
        self.money = money
        self.damage = damage
        self.protection = protection


enemy_1 = enemy('Troll', 10, 1, 1, 5, 0)
enemy_2 = enemy('Goblin', 13, 1, 1, 5, 1)
enemy_3 = enemy('Ogre', 25, 1, 1, 10, 5)
enemy_4 = enemy('Giant', 50, 5, 3, 20, 10)


def map_a():
    pass


def fight():
    global chosen_enemy
    chose_fight = input(
        "Which monster will you fight?\n1: Troll\n2: Goblin\n3: Ogre\n4: Giant\n")
    if chose_fight == "1":
        chosen_enemy = 1
        attack()
    if chose_fight == "2":
        chosen_enemy = 2
        attack()
    if chose_fight == "3":
        chosen_enemy = 3
        attack()
    if chose_fight == "4":
        chosen_enemy = 4
        attack()
    else:
        fight()


def attack():

    global mana
    global hp_hero
    global enemy_hp
    global money
    global potion
    global experience
    #global attack_hero
    global rage
    global giant_hp

    attack_random = (random.randint(1, 10))
    e_attack_random = (random.randint(1, 10))
    if weapon == 1:
        attack_hero = + 5 + (random.randint(1, 5))

    if chosen_enemy == 1:
        enemy_name = enemy_1.name
        enemy_hp = enemy_1.hp
        won_exp = enemy_1.exp
        won_money = enemy_1.money
        enemy_damage = enemy_1.damage
        enemy_protection = enemy_1.protection
    if chosen_enemy == 2:
        enemy_name = enemy_2.name
        enemy_hp = enemy_2.hp
        won_exp = enemy_2.exp
        won_money = enemy_2.money
        enemy_damage = enemy_2.damage
        enemy_protection = enemy_2.protection
    if chosen_enemy == 3:
        enemy_name = enemy_3.name
        enemy_hp = enemy_3.hp
        won_exp = enemy_3.exp
        won_money = enemy_3.money
        enemy_damage = enemy_3.damage
        enemy_protection = enemy_3.protection
    if chosen_enemy == 4:
        enemy_name = enemy_4.name
        enemy_hp = enemy_4.hp
        won_exp = enemy_4.exp
        won_money = enemy_4.money
        enemy_damage = enemy_4.damage
        enemy_protection = enemy_4.protection

        if shield == 0:
            enemy_damage = 6 + (random.randint(1, 5))
        if shield == 1:
            enemy_damage = 1 + (random.randint(1, 5))

        if hp_hero >= 1 and enemy_hp >= 1:
            att = input(
                "What will you do?\nEnter 1 to attack the " + enemy_name + " \nEnter 2 to use magic\nEnter 3 to take a potion\n ")
            if att == "1":
                if enemy_hp >= 1:
                    if attack_random <= 6:
                        enemy_hp = enemy_hp - attack_hero
                        if enemy_hp < 0:
                            enemy_hp = 0
                        print("The " + enemy_name + " loses " + str(attack_hero) +
                              "hp and have " + str(enemy_hp) + "hp left\n")
                        input()
                        if enemy_hp <= 0:
                            print("Victory!")
                            print("Experience + " + str(won_exp))
                            print("Money + " + str(won_money))
                            print("Rage points + " + str(rage))
                            money = money + won_money
                            experience = experience + won_exp
                            print("Hp = " + str(hp_hero))
                            input()
                            map_a()
                        if enemy_hp >= 1:
                            print("Now the " + enemy_name + " attacks")
                            input()
                            if e_attack_random <= 5:
                                hp_hero = hp_hero - enemy_damage
                                print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage += 1
                                input()
                                attack()
                            if e_attack_random <= 8:
                                hp_hero = hp_hero - enemy_damage * 2
                                print("Critical hit! You lose " + str(enemy_damage*2) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage = + 1
                                input()
                                attack()
                            if e_attack_random <= 10:
                                print("Miss! You have " +
                                      str(hp_hero) + "hp left\n")
                                input()
                                attack()

                    if attack_random <= 9:
                        enemy_hp = enemy_hp - attack_hero * 2
                        if enemy_hp < 0:
                            enemy_hp = 0
                        print("Critical hit! The " + enemy_name + " loses " + str(attack_hero*2) +
                              "hp and have " + str(enemy_hp) + "hp left\n")
                        input()
                        if enemy_hp <= 0:

                            print("Victory!")
                            print("Experience + " + str(won_exp))
                            print("Money + " + str(won_money))
                            money = money + won_money
                            experience = experience + won_money
                            print("Hp = " + str(hp_hero))
                            input()
                            fight()
                        if enemy_hp >= 1:
                            print("Now the " + enemy_name + " attacks")
                            input()
                            if e_attack_random <= 5:
                                hp_hero = hp_hero - enemy_damage
                                print("The " + enemy_name + " bites you and you lose " + str(enemy_damage) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage = + 1
                                input()
                                attack()
                            if e_attack_random <= 8:
                                hp_hero = hp_hero - enemy_damage * 2
                                print("Critical hit! You lose " + str(enemy_damage*2) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage = + 1
                                input()
                                attack()
                            if e_attack_random <= 10:
                                print("Miss! You have " +
                                      str(hp_hero) + "hp left\n")
                                input()
                                attack()
                    if attack_random == 10:

                        print("Miss! The " + enemy_name +
                              " have " + str(enemy_hp) + "hp left\n")
                        input()
                        if enemy_hp >= 1:
                            print("Now the " + enemy_name + " attacks")
                            input()
                            if e_attack_random <= 5:
                                hp_hero = hp_hero - enemy_damage
                                print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage = + 1
                                input()
                                attack()
                            if e_attack_random <= 8:
                                hp_hero = hp_hero - enemy_damage * 2
                                print("Critical hit! You lose " + str(enemy_damage*2) +
                                      "hp and have " + str(hp_hero) + "hp left\n")
                                rage = + 1
                                input()
                                attack()
                            if e_attack_random <= 10:
                                print("Miss! You have " +
                                      str(hp_hero) + "hp left\n")
                                input()
                                attack()

            if att == "2":
                if magic_level > 0:
                    if magic_level == 1:
                        spell1 = input(
                            "Enter h to use a heal spell x for exit ")
                        if spell1 == "h":
                            if mana >= 5:
                                hp_hero = hp_hero + heal
                                mana = mana - 5
                                print(
                                    "You used a heal spell and increased you hp to " + str(hp_hero))
                                input()
                                if enemy_hp >= 1:
                                    hp_hero = hp_hero - enemy_damage
                                    print("Now the " + enemy_name + " attacks")
                                    input()
                                    print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                                          "hp and have " + str(hp_hero) + "hp left\n")
                                    input()
                                    attack()
                            if mana < 5:
                                print("You don\'t have enough mana")
                                input()
                                attack()
                        if spell1 == "x":
                            attack()
                        else:
                            attack()
                    if magic_level == 2:
                        spell1 = input(
                            "Enter h to use a heal spell, f to use Fireball,  x for exit ")
                        if spell1 == "h":
                            if mana >= 5:
                                hp_hero = hp_hero + heal
                                mana = mana - 5
                                print(
                                    "You used a heal spell and increased you hp to " + str(hp_hero))
                                input()
                                if enemy_hp >= 1:
                                    hp_hero = hp_hero - enemy_damage
                                    print("Now the " + enemy_name + " attacks")
                                    input()
                                    print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                                          "hp and have " + str(hp_hero) + "hp left\n")
                                    input()
                                    attack()
                            if mana < 5:
                                print("You don\'t have enough mana")
                                input()
                                attack()
                        if spell1 == "f":
                            if mana >= 5:
                                enemy_hp = enemy_hp - fireball
                                mana = mana - 5
                                print(
                                    "You used a fireball spell and the " + enemy_name + " lose " + str(fireball)+" hp and have " + str(enemy_hp) + " left \n")
                                input()
                                if enemy_hp <= 0:
                                    print("Victory!")
                                    print("Experience + 1")
                                    print("Money + 1")
                                    money = money + 1
                                    enemy_hp = 25
                                    experience = experience + 1
                                    troll = troll - 1
                                    print("Hp = " + str(hp_hero))
                                    input()
                                    map_a()
                                if enemy_hp >= 1:
                                    hp_hero = hp_hero - enemy_damage
                                    print("Now the " + enemy_name + " attacks")
                                    input()
                                    print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                                          "hp and have " + str(hp_hero) + "hp left\n")
                                    input()
                                    attack()
                            if mana < 5:
                                print("You don\'t have enough mana")
                                input()
                                attack()
                        if spell1 == "x":
                            attack()

                else:
                    print("You don\'t have any spells yet")
                    input()

            if att == "3":
                if potion >= 1:
                    hp_hero = hp_hero + 30
                    print("You used a potion and your hp increased to " +
                          str(hp_hero) + "\n")
                    potion = potion - 1
                    hp_hero = hp_hero - enemy_damage
                    print("Now the " + enemy_name + " attacks")
                    input()
                    print("The " + enemy_name + " attacks you and you lose " + str(enemy_damage) +
                          "hp and have " + str(hp_hero) + "hp left\n")
                else:
                    print("You got no potions left\n")
            else:
                attack()
        if hp_hero <= 0:
            print("You died..... GAME OVER")
            input()

    # attack()
fight()
